package com.bcp.portal.Web.Page;

import com.bcp.portal.Util.UtilWeb;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.By;

public class PerurailPage extends PageObject {

    @FindBy(xpath = "//*[@id='destinoSelect']")
    WebElementFacade selectDestino;
    @FindBy(xpath = "//*[@id='rutaSelect']")
    WebElementFacade selectRuta;
    @FindBy(xpath = "//*[@id='cbTrenSelect']")
    WebElementFacade selectTren;
    @FindBy(xpath = "//*[@id='salida']")
    WebElementFacade calendar;
    @FindBy(xpath = "//*[@id='ui-datepicker-div']//*[@title='Next']")
    WebElementFacade flechaCalendar;
    @FindBy(xpath = "//*[@id='ui-datepicker-div']//*[@class='ui-datepicker-title']")
    WebElementFacade textCalendar;
    @FindBy(xpath = "//*[@id='btn_search']")
    WebElementFacade buttonBuscar;
    @FindBy(xpath = "//*[@name='selectRooms[suite]']")
    WebElementFacade selectCabina;
    @FindBy(xpath = "//*[@id='continuar_bae' or @class='btn  btn-continuar']")
    WebElementFacade buttonContinuar;
    @FindBy(xpath = "//*[@placeholder='First name' or @placeholder='Names']")
    WebElementFacade textNombre;
    @FindBy(xpath = "//*[@placeholder='Last name' or @placeholder='Surname']")
    WebElementFacade textApellido;
    @FindBy(xpath = "//*[@placeholder='Date of birth' or @id='formPasajero1-fecNacimiento']")
    WebElementFacade calendarNacimiento;
    @FindBy(xpath = "//*[@id='sel_nacion[suite][cab1][1]' or @id='formPasajero1-idPais']")
    WebElementFacade selectNacionalidad;
    @FindBy(xpath = "//*[@placeholder='Document number' or @placeholder='Example : 45244431']")
    WebElementFacade textNumeroDoc;
    @FindBy(xpath = "//*[@id='sel_sexo[suite][cab1][1]' or @id='formPasajero1-idSexo']")
    WebElementFacade selectGenero;
    @FindBy(xpath = "//*[@placeholder='Telephone']")
    WebElementFacade textTelefono;
    @FindBy(xpath = "//*[@placeholder='E-mail' or @id='formPasajero1-desEmail']")
    WebElementFacade textEmail;
    @FindBy(xpath = "//*[@placeholder='Confirm your email' or @id='formPasajero1-desEmailConfirmacion']")
    WebElementFacade textReEmail;
    @FindBy(xpath = "//input[@id='chk_ofertas[suite][cab1][1]' or @id='formPasajero1-recibirNovedades']")
    WebElementFacade checkAgree;
    @FindBy(xpath = "//*[@id='btnContinuarPas' or @id='enviarPago']")
    WebElementFacade buttonContinue;
    @FindBy(xpath = "//*[@id='chk_tercon' or @id='terms']")
    WebElementFacade checkTerminos;
    @FindBy(xpath = "//*[@id='btn_tarjeta' or @class='botones-inner']/input")
    WebElementFacade buttonYourCarNumber;
    @FindBy(xpath = "//*[@id='F1009']")
    WebElementFacade textNroTarjeta;
    @FindBy(xpath = "//*[@id='F1010_MM']")
    WebElementFacade selectMesTarjeta;
    @FindBy(xpath = "//*[@id='F1010_YY']")
    WebElementFacade selectAnioTarjeta;
    @FindBy(xpath = "//*[@id='F1136']")
    WebElementFacade textCV2;
    @FindBy(xpath = "//*[@id='F1142']")
    WebElementFacade textNombreTarjeta;
    @FindBy(xpath = "//*[@id='btnSubmit']")
    WebElementFacade buttonContinueFinish;
    @FindBy(xpath = "//*[@id='btnCancel']")
    WebElementFacade buttonCancelarFinish;
    @FindBy(xpath = "//*[@id='confirmacion' or @class='descripcion-pago']")
    WebElementFacade textEsperadoResult;
    @FindBy(xpath = "//*[@id='countParentsChildren']")
    WebElementFacade buttonPersonas;
    @FindBy(xpath = "//*[@id='adultsSelect']")
    WebElementFacade textPersonas;
    @FindBy(xpath = "//*[@id='adultsDism']")
    WebElementFacade buttonDismPersonas;
    @FindBy(xpath = "//*[@id='div_2021021699_12']//*[@class='divFila columnaIdaTripPR  double  columnaIdaTripPR informacionTrip']")
    WebElementFacade checkListOferta;
    private String iframe = "global",texto="";

    UtilWeb utilWeb;

    public boolean isPageRenderedProperly() {
        return selectDestino.isDisplayed();
    }

    public void seleccionarTipoViaje(String value) {
        utilWeb.clickByText("//*[@id='radioset']", "label", value);
    }

    public void seleccionarDestino(String value) {
        selectDestino.click();
        utilWeb.clickByText("//*[@id='destinoSelect']", "option", value);
    }

    public void seleccionarRuta(String value) {
        selectRuta.click();
        utilWeb.clickByText("//*[@id='rutaSelect']", "option", value);
    }

    public void seleccionarTren(String value) {
        //selectTren.click();
        getDriver().findElement(By.xpath("//*[@id='cbTrenSelect']")).click();
        utilWeb.clickByText("//*[@id='cbTrenSelect']", "option", value);
    }

    public void seleccionarFecha(String value) {
        calendar.click();
        for (int i = 0; i <= 60; i++) {
            if (textCalendar.getText().equalsIgnoreCase(value.substring(3)))
                break;
            flechaCalendar.click();
        }
        getDriver().findElement(By.xpath("//*[@data-handler='selectDay']/a[contains(.,'" + value.substring(0, 2) + "')]")).click();
    }

    public void clickButtonBuscar() {
        buttonBuscar.click();
    }

    public void seleccionarCabina(String value) {
        this.texto = value.substring(0, 1);
        selectCabina.click();
        utilWeb.clickByText("//*[@name='selectRooms[suite]']", "option", value);
    }

    public void seleccionarAdultos(String value) {
        for (int i = 1; i <= Integer.parseInt(texto); i++) {
            getDriver().findElement(By.xpath("//*[@name='selectRooms[suite][cabinas][cab" + i + "][adult]']")).click();
            utilWeb.clickByText("//*[@name='selectRooms[suite][cabinas][cab" + i + "][adult]']", "option", value);
        }
    }

    public void clickButtonContinuar() {
        buttonContinuar.click();
    }

    public void clickButtonFinish() {
        buttonContinueFinish.click();
    }

    public void ingresarNombre(String value) {
        textNombre.sendKeys(value);
    }

    public void ingresarApellido(String value) {
        textApellido.sendKeys(value);
    }

    public void seleccionarNacimiento(String value) {
        String[] split = value.split("/");
        calendarNacimiento.click();
        utilWeb.clickByText("//*[@class='ui-datepicker-month' or @id='calendario_mes']", "option", split[1]);
        utilWeb.clickByText("//*[@class='ui-datepicker-year' or @id='calendario_anio']", "option", split[2]);
        getDriver().findElement(By.xpath("//*[@data-handler='selectDay' or @style='background:#FFFFFF;']//a[(text()='" + split[0] + "')]")).click();
    }

    public void seleccionarNacionalidad(String value) {
        selectNacionalidad.click();
        utilWeb.clickByText("//*[@id='sel_nacion[suite][cab1][1]' or @id='formPasajero1-idPais']", "option", value);
    }

    public void seleccionarDocumento(String value) {
        getDriver().findElement(By.xpath("//*[@id='formPasajero1-idDocumentoIdentidad' or @id='sel_tpdoc[suite][cab1][1]']")).click();
        getDriver().findElement(By.xpath("//*[@id='formPasajero1-idDocumentoIdentidad' or @id='sel_tpdoc[suite][cab1][1]']//option[(text()='" + value + "')]")).click();
    }

    public void ingresarNumeroDoc(String value) {
        textNumeroDoc.sendKeys(value);
    }

    public void seleccionarGenero(String value) {
        selectGenero.click();
        utilWeb.clickByText("//*[@id='sel_sexo[suite][cab1][1]' or @id='formPasajero1-idSexo']", "option", value);
    }

    public void ingresarTelefono(String value) {
        textTelefono.sendKeys(value);
    }

    public void ingresarEmail(String value) {
        textEmail.sendKeys(value);
    }

    public void ingresarReEmail(String value) {
        textReEmail.sendKeys(value);
    }

    public void checkAgree() {
        checkAgree.click();
    }

    public void clickButtonContinue() {
        buttonContinue.click();
    }

    public void seleccionarMetodoPago(String value) {
        getDriver().findElement(By.xpath("//*[@id='" + value + "']")).click();
    }

    public void clickAgregarTerminos() {
        checkTerminos.click();
    }

    public void clickButtonYourCardNumber() {
        buttonYourCarNumber.click();
    }

    public void ingresarNroTarjeta(String value) {
        getDriver().switchTo().frame(iframe);
        textNroTarjeta.sendKeys(value);
    }

    public void seleccionarMesExpiracion(String value) {
        selectMesTarjeta.click();
        utilWeb.clickByText("//*[@id='F1010_MM']", "option", value);
    }

    public void seleccionarAnioExpiracion(String value) {
        selectAnioTarjeta.click();
        utilWeb.clickByText("//*[@id='F1010_YY']", "option", value);
    }

    public void ingresarNumeroCV2(String value) {
        textCV2.sendKeys(value);
    }

    public void ingresarNombreTarjeta(String value) {
        textNombreTarjeta.sendKeys(value);
    }

    public boolean validarResultadoEsperado() {
        textEsperadoResult.isPresent();
        boolean value = textEsperadoResult.isDisplayed();
        Serenity.takeScreenshot();
        return value;
    }

    public void clickButtonPersonas() {
        buttonPersonas.click();
    }

    public void seleccionarPersonas(String value) {
        for (int i = 0; i <= 60; i++) {
            if (textPersonas.getAttribute("value").equalsIgnoreCase(value))
                break;
            buttonDismPersonas.click();
        }
        getDriver().findElement(By.xpath("//*[@class='cerrar-passanger']")).click();
    }

    public void clickButtonOferta() {
        iframe = "iframeGC";
        checkListOferta.click();
    }

}