package com.bcp.portal.Web.Step;

import com.bcp.portal.Web.Page.PerurailPage;
import io.cucumber.java.es.*;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;
import org.junit.Assert;

public class PerurailSteps {

    PerurailPage perurailPage;
    EnvironmentVariables variables = SystemEnvironmentVariables.createEnvironmentVariables();

    @Dado("Que ingreso a la pagina de Peru Rail")
    public void queIngresoALaPaginaDePeruRail() {
        perurailPage.openAt(com.bcp.portal.Util.ConfigurationUtil.getContentEvironmentVariable(variables, "url.base.prod"));
        Assert.assertTrue(perurailPage.isPageRenderedProperly());
    }

    @Cuando("Selecciono el tipo de viaje: \"(.+)\"$")
    public void seleccionoElTipoDeViaje(String value) {
        this.perurailPage.seleccionarTipoViaje(value);
    }

    @Y("Selecciono Destino: \"(.+)\"$")
    public void seleccionoDestino(String value) {
        this.perurailPage.seleccionarDestino(value);
    }

    @Y("Selecciono la Ruta: \"(.+)\"$")
    public void seleccionarLaRuta(String value) {
        this.perurailPage.seleccionarRuta(value);
    }

    @Y("Selecciono el servicio de tren: \"(.+)\"$")
    public void seleccionarElServicioDeTren(String value) {
        this.perurailPage.seleccionarTren(value);
    }

    @Y("Selecciono la fecha de reservación: \"(.+)\"$")
    public void seleccionarLaFechaDeReservación(String value) {
        this.perurailPage.seleccionarFecha(value);
    }

    @Y("Presiono click en Find Train Tickets")
    public void damosClickEnFindTrainTickets() {
        this.perurailPage.clickButtonBuscar();
    }

    @Y("Selecciono la cantidad de cabinas \"(.+)\" de suite cabins$")
    public void seleccionamosLaCantidadDeCabinasDeSuiteCabins(String value) {
        this.perurailPage.seleccionarCabina(value);
    }

    @Y("Selecciono la cantidad de adultos \"(.+)\"$")
    public void seleccionamosLaCantidadDeAdultos(String value) {
        this.perurailPage.seleccionarAdultos(value);
    }

    @Y("Presiono click en el boton continuar")
    public void damosClickEnElBotonContinuar() {
        this.perurailPage.clickButtonContinuar();
    }

    @Y("Ingreso el nombre del pasajero: \"(.+)\"$")
    public void ingresamosElNombreDelPasajero(String value) {
        this.perurailPage.ingresarNombre(value);
    }

    @Y("Ingreso el apellido del pasajero: \"(.+)\"$")
    public void ingresamosElApellidoDelPasajero(String value) {
        this.perurailPage.ingresarApellido(value);
    }

    @Y("Ingreso fecha de cumpleaños \"(.+)\"$")
    public void ingresarFechaDeCumpleanios(String value) {
        this.perurailPage.seleccionarNacimiento(value);
    }

    @Y("Selecciono la nacionalidad: \"(.+)\"$")
    public void seleccionamosLaNacionalidad(String value) {
        this.perurailPage.seleccionarNacionalidad(value);
    }

    @Y("Selecciono el tipo de documento: \"(.+)\"$")
    public void seleccionamosElTipoDeDocumento(String value) {
        this.perurailPage.seleccionarDocumento(value);
    }

    @Y("Ingreso el numero de documento: \"(.+)\"$")
    public void ingresamosElNumeroDeDocumento(String value) {
        this.perurailPage.ingresarNumeroDoc(value);
    }

    @Y("Ingreso el tipo de sexo: \"(.+)\"$")
    public void ingresamosElTipoDeSexo(String value) {
        this.perurailPage.seleccionarGenero(value);
    }

    @Y("Ingreso nro de celular: \"(.+)\"$")
    public void ingresarNroDeCelular(String value) {
        this.perurailPage.ingresarTelefono(value);
    }

    @Y("Ingreso el correo: \"(.+)\"$")
    public void ingresamosElCorreo(String value) {
        this.perurailPage.ingresarEmail(value);
    }

    @Y("Confirmo el correo: \"(.+)\"$")
    public void confirmamosElCorreo(String value) {
        this.perurailPage.ingresarReEmail(value);
    }

    @Y("Presiono click en el boton agree")
    public void darClickEnElBotonAgree() {
        this.perurailPage.checkAgree();
    }

    @Y("Presiono click en continuar")
    public void damosClickEnContinuar() {
        this.perurailPage.clickButtonContinue();
    }

    @Y("Selecciono el metodo de pago \"(.+)\"$")
    public void seleccionoElMetodoDePago(String value) {
        this.perurailPage.seleccionarMetodoPago(value);
    }

    @Y("Selecciono los terminos y condiciones")
    public void seleccionoLosTerminosYCondiciones() {
        this.perurailPage.clickAgregarTerminos();
    }

    @Y("Presiono click en el boton Enter your card number")
    public void darClickEnElBotonEnterYourCardNumber() {
        this.perurailPage.clickButtonYourCardNumber();
    }

    @Y("Ingreso numero de tarjeta: \"(.+)\"$")
    public void ingresarNumeroDeTarjeta(String value) {
        this.perurailPage.ingresarNroTarjeta(value);
    }

    @Y("Selecciono el mes de expiracion: \"(.+)\"$")
    public void seleccionamosElMesDeExpiracion(String value) {
        this.perurailPage.seleccionarMesExpiracion(value);
    }

    @Y("Selecciono el año de expiracion: \"(.+)\"$")
    public void seleccionamosElAñoDeExpiracion(String value) {
        this.perurailPage.seleccionarAnioExpiracion(value);
    }

    @Y("Ingreso el codigo de seguridad: \"(.+)\"$")
    public void ingresarElCodigoDeSeguridad(String value) {
        this.perurailPage.ingresarNumeroCV2(value);
    }

    @Y("Ingreso nombre en la tarjeta: \"(.+)\"$")
    public void ingresarNombreEnLaTarjeta(String value) {
        this.perurailPage.ingresarNombreTarjeta(value);
    }

    @Y("Presiono click en el boton para finalizar")
    public void darClickEnElBotonContinuar() {
        this.perurailPage.clickButtonFinish();
    }

    @Entonces("Valido resultado esperado")
    public void validarResultadoEsperado() {
        Assert.assertTrue(this.perurailPage.validarResultadoEsperado());
    }

    @Y("Presiono click para visualizar la ventana de cantidad")
    public void darClickParaVisualizarLaVentanaDeCantidad() {
        this.perurailPage.clickButtonPersonas();
    }

    @Y("Selecciono la cantidad de personas: \"(.+)\"$")
    public void seleccionoLaCantidadDePersonas(String value) {
        this.perurailPage.seleccionarPersonas(value);
    }

    @Y("Selecciono el precio de viaje oferta")
    public void seleccionamosElPrecioDeViajeOferta() {
        this.perurailPage.clickButtonOferta();
    }

}