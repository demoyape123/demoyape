package com.bcp.portal.JiraXray.dom;

import io.restassured.http.Header;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.rest.SerenityRest;
import java.io.File;

public class JiraXrayServiceBase {

    public JiraXrayServiceBase() {
    }

    public Response importTestResultExecution(String pathResource, String pathJsonFile, String user, String password) {
        Response response = (Response) ((RequestSpecification) SerenityRest.given().header(new Header("Content-Type", "application/json")).body(new File(pathJsonFile)).auth().preemptive().basic(user, password).when().log().all()).post(pathResource + "/rest/raven/1.0/import/execution/cucumber", new Object[0]);
        response.prettyPeek();
        return response;
    }

}