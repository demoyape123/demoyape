package com.bcp.portal.JiraXray.help;

import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.util.EnvironmentVariables;

public class HelperCredentials {

    private EnvironmentVariables environmentVariables;

    public HelperCredentials(EnvironmentVariables environmentVariables) {
        this.environmentVariables = environmentVariables;
    }

    public String getJXrayUser() {
        return EnvironmentSpecificConfiguration.from(this.environmentVariables).getProperty("jxray.user");
    }

    public String getJXrayPassword() {
        return EnvironmentSpecificConfiguration.from(this.environmentVariables).getProperty("jxray.pass");
    }

    public String getPathResource() {
        return EnvironmentSpecificConfiguration.from(this.environmentVariables).getProperty("jxray.base.url");
    }

}