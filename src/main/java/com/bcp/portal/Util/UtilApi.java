package com.bcp.portal.Util;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Predicate;
import com.jayway.jsonpath.spi.json.JsonOrgJsonProvider;
import com.jayway.jsonpath.spi.mapper.JsonOrgMappingProvider;
import io.cucumber.datatable.DataTable;
import org.jetbrains.annotations.NotNull;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;

public class UtilApi {

    public static String getValueFromDataTable(DataTable dataTable, String title) {
        List<Map<String, String>> list = dataTable.asMaps();
        return (String) ((Map) list.get(0)).get(title);
    }

    public static String updateValueOfNodeInJson(String jsonString, String keyPath, String value) {
        Configuration configuration = Configuration.builder().jsonProvider(new JsonOrgJsonProvider()).mappingProvider(new JsonOrgMappingProvider()).build();
        return JsonPath.using(configuration).parse(jsonString).set("$." + keyPath, value, new Predicate[0]).jsonString();
    }

    public static String sendCurrentDate() {
        return ZonedDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ"));
    }

    public static String sendCurrentUuid() {
        return UUID.randomUUID().toString();
    }

    public static String locationProject() {
        return System.getProperty("user.dir");
    }

    public static Logger logger(@NotNull Class clase) {
        return Logger.getLogger(clase.getName());
    }

}