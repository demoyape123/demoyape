package com.bcp.portal.Util.ApiManager.generic;

import io.cucumber.datatable.DataTable;
import io.restassured.http.Headers;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import com.bcp.portal.Util.ApiManager.builder.ApiConfig;
import java.util.Map;

public interface IServiceBase {

    Headers configurarHeaders(DataTable var1);

    Map<String, Object> configurarParams(DataTable var1);

    String configurarBodyRequest(String var1, DataTable var2);

    Response ejecutarApiBuilder(ApiConfig var1);

    ValidatableResponse validarCodigoRespuesta(Response var1, int var2);

}