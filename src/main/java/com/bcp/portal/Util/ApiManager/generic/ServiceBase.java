package com.bcp.portal.Util.ApiManager.generic;

import io.cucumber.datatable.DataTable;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.util.EnvironmentVariables;
import org.apache.commons.io.FileUtils;
import com.bcp.portal.Util.ApiManager.builder.ApiConfig;
import com.bcp.portal.Util.ApiManager.builder.ApiConfigBuilder;
import com.bcp.portal.Util.ApiManager.constants.ConstantsApiConf;
import com.bcp.portal.Util.UtilApi;
import org.json.JSONObject;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;

public class ServiceBase implements IServiceBase {

    private final String sourceClass = this.getClass().getName();
    private transient RequestSpecification reqConfig = null;
    private transient EnvironmentVariables environmentVariables;
    private final transient Map<String, Object> parameters = new HashMap<>();
    private final transient Map<String, Object> multipart = new HashMap<>();
    private String body = null;

    protected ApiConfigBuilder apiBuilder() {
        return new ApiConfigBuilder();
    }

    @Override
    public Response ejecutarApiBuilder(ApiConfig apiConfig) {
        SerenityRest.useRelaxedHTTPSValidation();
        Response response = null;
        Map<String, Object> pathVariables = Collections.emptyMap();
        Map<String, Object> parametros = Collections.emptyMap();
        Map<String, Object> formUrlEncoded = Collections.emptyMap();
        Map<String, Object> formdata = Collections.emptyMap();
        String body = "";
        String typeMethod = apiConfig.getMethod().toUpperCase();
        String typeAPI = apiConfig.getApiType();
        String ApiURL = apiConfig.getApiURL();
        Headers headers = apiConfig.getHeaders();
        String fullURL;
        String baseURI;
        baseURI = EnvironmentSpecificConfiguration.from(this.environmentVariables).getProperty(ConstantsApiConf.URLBASE + typeAPI);
        fullURL = baseURI + ApiURL;
        if (apiConfig.getPathVariables() != null)
            pathVariables = apiConfig.getPathVariables();
        if (apiConfig.getParams() != null)
            parametros = apiConfig.getParams();
        if (apiConfig.getFormUrlEncoded() != null)
            formUrlEncoded = apiConfig.getFormUrlEncoded();
        if (apiConfig.getFormData() != null)
            formdata = apiConfig.getFormData();
        if (apiConfig.getBody() != null)
            body = apiConfig.getBody();
        try {
            baseURI = typeMethod.toUpperCase(Locale.getDefault());
            switch (baseURI) {
                case ConstantsApiConf.GET:
                    this.reqConfig = SerenityRest.given().headers(headers).pathParams(pathVariables).params(parametros);
                    response = (Response) ((RequestSpecification) this.reqConfig.when().log().all()).get(fullURL, new Object[0]);
                    break;
                case ConstantsApiConf.POST:
                    this.reqConfig = SerenityRest.given().headers(headers);
                    if (body.isEmpty()) {
                        this.reqConfig.formParams(parametros);
                    } else {
                        this.reqConfig.body(body);
                    }
                    if (apiConfig.getFormData() != null) {
                        File file = new File(UtilApi.locationProject() + formdata.values().toString().replace("[", "").replace("]", ""));
                        this.reqConfig.multiPart(formdata.keySet().toString().replace("[", "").replace("]", ""), file);
                    }
                    response = (Response) ((RequestSpecification) this.reqConfig.when().log().all()).post(fullURL, new Object[0]);
                    break;
            }
        } catch (Exception e) {
            UtilApi.logger(ServiceBase.class).log(Level.WARNING, "Cause {0}", e.getCause());
            UtilApi.logger(ServiceBase.class).log(Level.WARNING, "Message {0}", e.getMessage());
            UtilApi.logger(ServiceBase.class).throwing(this.sourceClass, "ejecutarApiBuilder()", e);
        }
        assert response != null;
        response.prettyPeek();
        return response;
    }

    public ValidatableResponse validarCodigoRespuesta(Response response, int statuscode) {
        return (ValidatableResponse) ((ValidatableResponse) ((ValidatableResponse) response.then()).assertThat()).statusCode(statuscode);
    }

    @Override
    public Headers configurarHeaders(DataTable dataTable) {
        List<Map<String, String>> list = dataTable.asMaps();
        List<Header> headerList = new LinkedList<>();
        for (Map<String, String> stringStringMap : list) {
            Header header = new Header(stringStringMap.get(ConstantsApiConf.HEADER), stringStringMap.get(ConstantsApiConf.VALUE));
            if (!header.getValue().contains(ConstantsApiConf.EMPTY)) {
                headerList.add(header);
            } else {
                UtilApi.logger(this.getClass()).log(Level.INFO, "No se esta empleando headers configurados y/o se esta usando %EMPTY%");
            }
        }

        Headers headers = new Headers(headerList);
        UtilApi.logger(ServiceBase.class).log(Level.INFO, "HEADERS: {0}", headers);
        return headers;
    }

    @Override
    public Map<String, Object> configurarParams(DataTable dataTable) {
        List<Map<String, String>> list = dataTable.asMaps();
        for (Map<String, String> stringMap : list) {
            if (!stringMap.get(ConstantsApiConf.PARAMS).contains(ConstantsApiConf.EMPTY)) {
                this.parameters.put(stringMap.get(ConstantsApiConf.PARAMS), stringMap.get(ConstantsApiConf.VALUE));
            } else {
                UtilApi.logger(this.getClass()).log(Level.INFO, "No se esta empleando path-variables y/o se esta usando %EMPTY%");
            }
        }

        UtilApi.logger(ServiceBase.class).log(Level.INFO, "PARAMS: {0}", this.parameters);
        return this.parameters;
    }

    @Override
    public String configurarBodyRequest(String pathServiceRequest, DataTable dataTableRequestValues) {
        pathServiceRequest = System.getProperty("user.dir") + "/src/main/java/com/bcp/portal/Util/ApiManager/request/" + pathServiceRequest;
        File file = new File(pathServiceRequest);
        String bodyRequest = "";

        try {
            String content = FileUtils.readFileToString(file, ConstantsApiConf.UTF_8);
            JSONObject jsonObject = new JSONObject(content);
            bodyRequest = jsonObject.toString();
            List<Map<String, String>> listBodyRequest = dataTableRequestValues.asMaps();

            String newJson;
            for (Iterator<Map<String, String>> var = listBodyRequest.iterator(); var.hasNext(); bodyRequest = newJson) {
                Map<String, String> stringStringMap = var.next();
                newJson = UtilApi.updateValueOfNodeInJson(bodyRequest, (String) stringStringMap.get(ConstantsApiConf.KEY), ((String) stringStringMap.get(ConstantsApiConf.VALUE)).replace(ConstantsApiConf.BLANCO, " ").replace(ConstantsApiConf.BAR, "|"));
            }
        } catch (IOException e) {
            UtilApi.logger(this.getClass()).throwing(this.sourceClass, "configurerBodyRequest()", e);
        }

        UtilApi.logger(this.getClass()).log(Level.INFO, "BODY-REQUEST: {0}", bodyRequest);
        return bodyRequest;
    }

    public String obtenerBodyRequestTextoPlano(DataTable dataTable) {
        List<Map<String, String>> list = dataTable.asMaps();
        body = "{";
        for (int i = 0; i < list.size(); i++) {
            body = body + "\"" + (String) ((Map) list.get(i)).get("body") + "\": \"" + (String) ((Map) list.get(i)).get("valor") + "\", ";
        }
        body = body.substring(0, body.length() - 2) + "}";
        UtilApi.logger(ServiceBase.class).log(Level.INFO, "BODY-REQUEST: {0}", body);
        return body;
    }

}