package com.bcp.portal.Util;

import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;

public class UtilWeb extends PageObject {

    public boolean clickByText(String elementoContenedor, String etiqueta, String texto) {
        try {
            List<WebElement> LISTASELEMENTS = getDriver().findElements(By.xpath(elementoContenedor));
            for (WebElement contenedor : LISTASELEMENTS) {
                getDriver().findElement(By.xpath(elementoContenedor)).isDisplayed();
                try {
                    WebDriverWait wait = new WebDriverWait(getDriver(), 30);
                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementoContenedor)));
                } catch (Exception e) {
                    return false;
                }

                List<WebElement> listaDivs = contenedor.findElements(By.xpath(".//" + etiqueta));
                String textoValor = "";
                for (WebElement div : listaDivs) {
                    textoValor = div.getText();
                    if (textoValor.equalsIgnoreCase(texto)) {
                        div.click();
                        Thread.sleep(500);
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            return false;
        }
        return false;
    }

}
