package com.bcp.portal.Util;

import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.util.EnvironmentVariables;

public class ConfigurationUtil extends PageObject {

    public static String getContentEvironmentVariable(EnvironmentVariables environmentVariables, String propiedad) {
        return EnvironmentSpecificConfiguration.from(environmentVariables).getProperty(propiedad);
    }

}