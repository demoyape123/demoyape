package com.bcp.portal.Api.step;

import com.bcp.portal.Util.ApiManager.generic.ServiceBase;
import io.cucumber.datatable.DataTable;
import io.restassured.http.Headers;
import io.restassured.response.Response;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.util.EnvironmentVariables;
import com.bcp.portal.Util.ApiManager.builder.ApiConfig;
import com.bcp.portal.Util.ApiManager.constants.ConstantsApiConf;

import java.util.HashMap;
import java.util.Map;

import static com.bcp.portal.Util.UtilApi.getValueFromDataTable;

public class ApiSteps extends ServiceBase {
    private Response response;
    private Headers headers;
    private String bodyRequest;
    private Map<String, Object> params;
    private EnvironmentVariables environmentVariables;
    private HashMap<String, Object> nodes = new HashMap<>();

    @Step("configurar cabeceras")
    public void configurar_headers(DataTable dataTable) {
        headers = configurarHeaders(dataTable);
    }

    @Step("configurar parametros")
    public void configurar_params(DataTable dataTable) {
        params = configurarParams(dataTable);
    }

    @Step("Configurar el body request")
    public void bodyRequest(DataTable dataTable) {
        bodyRequest = obtenerBodyRequestTextoPlano(dataTable);
    }

    @Step("Configurar el body request para enviarlo")
    public void bodyRequestFormatoRaw(String path, DataTable dataTable) {
        bodyRequest = configurarBodyRequest(path, dataTable);
    }

    @Step("ejecutar api")
    public void ejecutar_api(DataTable dataTable) {
        String type = getValueFromDataTable(dataTable, ConstantsApiConf.TIPO_API);
        String url = getValueFromDataTable(dataTable, ConstantsApiConf.API_URL);
        String method = getValueFromDataTable(dataTable, ConstantsApiConf.METODO);
        String exec_url = EnvironmentSpecificConfiguration.from(environmentVariables).getProperty(ConstantsApiConf.URLBASEAPI + type + "." + url);
        ApiConfig apiConfig = apiBuilder()
                .withApiType(type)
                .withApiURL(exec_url)
                .withMethod(method)
                .withHeaders(headers)
                .withParams(params)
                .withBody(bodyRequest).build();
        response = ejecutarApiBuilder(apiConfig);
    }

    @Step("validar codigo de respuesta")
    public void validar_codigo(int codigoRespuesta) {
        validarCodigoRespuesta(response, codigoRespuesta);
    }

    @Step("Obtener valor del nodo de la respuesta")
    public String obtenerValorNodoRespuesta(String jsonPathNodo) {
        return response.getBody().jsonPath().getJsonObject(jsonPathNodo).toString();
    }

}