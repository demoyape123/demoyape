package com.bcp.portal.Api.stepdefinition;

import com.bcp.portal.Api.step.ApiSteps;
import com.bcp.portal.Util.ApiManager.constants.ConstantsApiConf;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.es.Dado;
import io.cucumber.java.es.Entonces;
import io.cucumber.java.es.Y;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.steps.ScenarioSteps;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

public class ApiStepDefinitions extends ScenarioSteps {

    @Steps
    private static ApiSteps apiSteps;

    @Dado("que configuro las cabeceras")
    public void configurarHeaders(DataTable dataTable) {
        apiSteps.configurar_headers(dataTable);
    }

    @Y("que configuro los parametros")
    public void configurarParametros(DataTable dataTable) {
        apiSteps.configurar_params(dataTable);
    }

    @Y("ejecuto el api")
    public void ejecutar_api(DataTable dataTable) {
        apiSteps.ejecutar_api(dataTable);
    }

    @Entonces("^valido si el codigo de respuesta es \"([^\"]*)\"$")
    public void valido_codigo_respuesta(int codigoRespuesta) {
        apiSteps.validar_codigo(codigoRespuesta);
    }

    @Dado("^que configuro el body request del servicio: \"([^\"]*)\"$")
    public void configuro_request_servicio(String path, DataTable dataTable) {
        apiSteps.bodyRequestFormatoRaw(path, dataTable);
    }

    @Y("que configuro el body request")
    public void configurarBodyTextPlain(DataTable dataTable) {
        apiSteps.bodyRequest(dataTable);
    }

    @Entonces("valido los siguientes datos en el json de respuesta")
    public void valido_codigo_respuesta(DataTable dataTable) {
        List<Map<String, String>> list = dataTable.asMaps();
        for (Map<String, String> stringStringMap : list) {
            String node = stringStringMap.get(ConstantsApiConf.NODO);
            String valor = stringStringMap.get(ConstantsApiConf.VALOR);
            Assert.assertEquals(valor, apiSteps.obtenerValorNodoRespuesta(node));
        }
    }

    @Y("obtengo los valores del body response")
    public void obtengoLosValoresDelResponse(DataTable dataTable) {
        List<Map<String, String>> list = dataTable.asMaps();
        for (Map<String, String> stringStringMap : list) {
            String node = stringStringMap.get(ConstantsApiConf.NODO);
            System.out.println(apiSteps.obtenerValorNodoRespuesta(node));
        }
    }

}