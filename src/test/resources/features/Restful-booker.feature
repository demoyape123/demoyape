#language: es
@APIS
Característica: API Services
  Como usuario comun
  Yo quiero ejecutar apis
  Para validar que respondan correctamente

  @PING
  Escenario: Validar que responda correctamente el api PING
    Dado que configuro las cabeceras
      | cabeceras | valor |
    Y que configuro los parametros
      | parametros | valor |
    Cuando ejecuto el api
      | tipo | url  | metodo |
      | jwt  | ping | GET    |
    Entonces valido si el codigo de respuesta es "201"

  @BOOKING
  Escenario: Validar que responda correctamente el api BOOKING
    Dado que configuro las cabeceras
      | cabeceras | valor |
    Y que configuro los parametros
      | parametros | valor |
    Cuando ejecuto el api
      | tipo | url     | metodo |
      | jwt  | booking | GET    |
    Entonces valido si el codigo de respuesta es "200"

  @AUTH
  Escenario: Validar que responda de manera correcta con usuario y clave validos el api AUTH
    Dado que configuro las cabeceras
      | cabeceras    | valor            |
      | Content-Type | application/json |
    Y que configuro el body request del servicio: "bodyAuth.json"
      | key | valor |
    Cuando ejecuto el api
      | tipo | url  | metodo |
      | jwt  | auth | POST   |
    Entonces valido si el codigo de respuesta es "200"
    Y obtengo los valores del body response
      | nodo  |
      | token |

  @AUTH_UnHappy
  Escenario: Validar que responda de manera incorrecta con usuario y clave invalidos el api AUTH
    Dado que configuro las cabeceras
      | cabeceras    | valor            |
      | Content-Type | application/json |
    Y que configuro el body request
      | body     | valor    |
      | admin    | admin    |
      | password | password |
    Cuando ejecuto el api
      | tipo | url  | metodo |
      | jwt  | auth | POST   |
    Entonces valido si el codigo de respuesta es "200"
    Y valido los siguientes datos en el json de respuesta
      | nodo   | valor           |
      | reason | Bad credentials |