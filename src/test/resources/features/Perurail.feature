#language: es

@WEB
Característica: Peru Rail
  Como un usuario comun
  Quiero ingresar mis datos
  Para obtener un pasaje de viaje

  Antecedentes:
    Dado Que ingreso a la pagina de Peru Rail

  @COMPRAR_PASAJES_01
  Escenario: Realizar la reserva de pasaje  s en Peru Rail BELMOND ANDEAN EXPLORER destino CUSCO y la ruta PUNO > CUSCO
    Cuando Selecciono el tipo de viaje: "One Way"
    Y Selecciono Destino: "<ubicacion>"
    Y Selecciono la Ruta: "Puno > Cusco"
    Y Selecciono el servicio de tren: "Andean Explorer, A Belmond Train"
    Y Selecciono la fecha de reservación: "15 June 2022"
    Y Presiono click en Find Train Tickets
    Y Selecciono la cantidad de cabinas "1 CABIN" de suite cabins
    Y Selecciono la cantidad de adultos "1"
    Y Presiono click en el boton continuar
    Y Ingreso el nombre del pasajero: "Arturo"
    Y Ingreso el apellido del pasajero: "Rodriguez"
    Y Ingreso fecha de cumpleaños "3/Mar/1971"
    Y Selecciono la nacionalidad: "Peru"
    Y Selecciono el tipo de documento: "Identification Card"
    Y Ingreso el numero de documento: "46819012"
    Y Ingreso el tipo de sexo: "Male"
    Y Ingreso nro de celular: "957576911"
    Y Ingreso el correo: "arodriva@gmail.com"
    Y Confirmo el correo: "arodriva@gmail.com"
    Y Presiono click en el boton agree
    Y Presiono click en continuar
    Y Selecciono el metodo de pago "visa"
    Y Selecciono los terminos y condiciones
    Y Presiono click en el boton Enter your card number
    Y Ingreso numero de tarjeta: "4546400034748181"
    Y Selecciono el mes de expiracion: "09"
    Y Selecciono el año de expiracion: "24"
    Y Ingreso el codigo de seguridad: "786"
    Y Ingreso nombre en la tarjeta: "Arturo Rodriguez"
    Y Presiono click en el boton para finalizar
    Entonces Valido resultado esperado

  @COMPRAR_PASAJES_02
  Escenario: Realizar la reserva de pasajes en Peru Rail BELMOND ANDEAN EXPLORER destino CUSCO y la ruta AREQUIPA > PUNO > CUSCO
    Cuando Selecciono el tipo de viaje: "One Way"
    Y Selecciono Destino: "Cusco"
    Y Selecciono la Ruta: "Arequipa > Puno > Cusco"
    Y Selecciono la fecha de reservación: "12 November 2022"
    Y Presiono click en Find Train Tickets
    Y Selecciono la cantidad de cabinas "1 CABIN" de suite cabins
    Y Selecciono la cantidad de adultos "1"
    Y Presiono click en el boton continuar
    Y Ingreso el nombre del pasajero: "Arturo"
    Y Ingreso el apellido del pasajero: "Rodriguez"
    Y Ingreso fecha de cumpleaños "3/Mar/1971"
    Y Selecciono la nacionalidad: "Peru"
    Y Selecciono el tipo de documento: "Identification Card"
    Y Ingreso el numero de documento: "46819012"
    Y Ingreso el tipo de sexo: "Male"
    Y Ingreso nro de celular: "957576911"
    Y Ingreso el correo: "arodriva@gmail.com"
    Y Confirmo el correo: "arodriva@gmail.com"
    Y Presiono click en el boton agree
    Y Presiono click en continuar
    Y Selecciono el metodo de pago "visa"
    Y Selecciono los terminos y condiciones
    Y Presiono click en el boton Enter your card number
    Y Ingreso numero de tarjeta: "4546400034748181"
    Y Selecciono el mes de expiracion: "09"
    Y Selecciono el año de expiracion: "24"
    Y Ingreso el codigo de seguridad: "786"
    Y Ingreso nombre en la tarjeta: "Arturo Rodriguez"
    Y Presiono click en el boton para finalizar
    Entonces Valido resultado esperado

  @COMPRAR_PASAJES_03
  Escenario: Realizar la reserva de pasajes en Peru Rail EXPEDITION destino MACHU PICCHU y la ruta CUSCO > MACHU PICCHU
    Cuando Selecciono el tipo de viaje: "One Way"
    Y Selecciono Destino: "Machu Picchu"
    Y Selecciono la Ruta: "Cusco > Machu Picchu"
    Y Selecciono la fecha de reservación: "22 July 2022"
    Y Presiono click para visualizar la ventana de cantidad
    Y Selecciono la cantidad de personas: "1"
    Y Presiono click en Find Train Tickets
    Y Selecciono el precio de viaje oferta
    Y Presiono click en el boton continuar
    Y Ingreso el nombre del pasajero: "Arturo"
    Y Ingreso el apellido del pasajero: "Rodriguez"
    Y Ingreso fecha de cumpleaños "3/March/1971"
    Y Selecciono la nacionalidad: "Peru"
    Y Selecciono el tipo de documento: "Identification Card"
    Y Ingreso el numero de documento: "46819012"
    Y Ingreso el tipo de sexo: "Male"
    Y Ingreso nro de celular: "957576911"
    Y Ingreso el correo: "arodriva@gmail.com"
    Y Confirmo el correo: "arodriva@gmail.com"
    Y Presiono click en el boton agree
    Y Presiono click en continuar
    Y Selecciono el metodo de pago "visa"
    Y Selecciono los terminos y condiciones
    Y Presiono click en el boton Enter your card number
    Y Ingreso numero de tarjeta: "4546400034748181"
    Y Selecciono el mes de expiracion: "09"
    Y Selecciono el año de expiracion: "24"
    Y Ingreso el codigo de seguridad: "786"
    Y Ingreso nombre en la tarjeta: "Arturo Rodriguez"
    Y Presiono click en el boton para finalizar
    Entonces Valido resultado esperado