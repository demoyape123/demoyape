package com.bcp.portal;

import com.bcp.portal.JiraXray.dom.JiraXrayServiceBase;
import com.bcp.portal.JiraXray.help.HelperCredentials;
import java.util.logging.Level;
import com.bcp.portal.Util.UtilApi;
import io.cucumber.junit.CucumberOptions;
import io.restassured.response.Response;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

/**
 * @author Adolfo A. Rodriguez
 * @description RUNNER QUE CONTROLA LAS EJECUCIONES
 */

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = {"pretty", "json:target/build/cucumber.json"},
        stepNotifications = true,
        features = "src/test/resources/features",
        tags = "@COMPRAR_PASAJES_01"
)

public class Runner {

    public static EnvironmentVariables environmentVariables;

    @BeforeClass
    public static void beforExecution() {
        environmentVariables = SystemEnvironmentVariables.createEnvironmentVariables();
    }

    @AfterClass
    public static void afterExecution() {
        UtilApi.logger(Runner.class).info("afterExecution()");
        if (Boolean.parseBoolean(com.bcp.portal.Util.ConfigurationUtil.getContentEvironmentVariable(environmentVariables, "jxray.update.evidence"))) {
            UtilApi.logger(Runner.class).log(Level.INFO, "Actualizar resultado en JiraXray: {0}", true);
            JiraXrayServiceBase jiraXrayServiceBase = new JiraXrayServiceBase();
            Response response = jiraXrayServiceBase.importTestResultExecution(
                    new HelperCredentials(environmentVariables).getPathResource(), System.getProperty("user.dir") + "/target/build/cucumber.json",
                    new HelperCredentials(environmentVariables).getJXrayUser(),
                    new HelperCredentials(environmentVariables).getJXrayPassword());
            response.then().assertThat().statusCode(200);
        } else {
            UtilApi.logger(Runner.class).log(Level.INFO, "Actualizar resultado en JiraXray: {0}", false);
        }
    }

}