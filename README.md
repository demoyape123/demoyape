# FFDY-test-automation-frontend

Framework de trabajo con Serenity, Cucumber y Maven

Requisitos
- El proyecto corre usando JDK 11 y tener configurada la respectiva variable de entorno
- El proyecto se complementa con Maven y su respectiva configuración de la variable de entorno
- Recordar configurar los usuarios de los paises con los que desean probar en el `serenity.properties`

Antes de ejecutar el proyecto se debe configurar el driver de tu elección. Posteriormente modificar el archivo `serenity.properties` para que descargue automáticamente el driver elegido:

- webdriver.driver = chrome

Tambien puedes eliminar la ruta del driver del archivo y configurarla en el path de tu Sistema Operativo. (Opción recomendada por la documentación de SerenityBDD)

### Instrucciones de Ejecución de Pruebas Automatizadas Web
Para realizar la ejecución de los features, se deberá identificar los **@tags** de los Features y ejecutar el siguiente comando:

***
En caso se desee ejecutar algún feature en particular se deberia indicar el tag de dicho feature
```
mvn clean verify "-Dcucumber.filter.tags=@LoginPortal"
```
***
En caso se deseen ejecutar todos los escenarios al mismo tiempo
```
mvn clean verify
```
***

El reporte de Serenity es generado en el directorio `target/site/serenity`